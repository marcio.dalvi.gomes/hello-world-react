import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Olá Mundo</Text>
      <Text style={styles.subtitle}>by Marcio.gomes</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title:{
    color: 'red',
    fontSize: 50,
    fontStyle: 'italic'
  },
  subtitle:{
    color: 'green'
  }
});
